package com.br.lead.collector.services;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.LeadRepository;
import com.br.lead.collector.repositories.ProdutoRepository;
import com.sun.xml.internal.ws.policy.AssertionSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTests {

    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadService leadService;

    Lead lead;

    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("vinicius@gmail.com");
        lead.setNome("Vinicius");
        lead.setProdutos(Arrays.asList(new Produto()));
    }

    @Test
    public void testarAdicionarLead(){

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadService.adicionarLead(lead);

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getEmail(),"vinicius@gmail.com");
    }

    @Test
    public void testarDeletarLead(){

        leadService.deletarLead(lead);
        Mockito.verify(leadRepository).delete(Mockito.any(Lead.class));
    }

    @Test
    public void testarBuscarLead() throws Exception{

        Optional<Lead> leadOptional = Optional.of(lead);

        leadService.buscarPorId(leadOptional.get().getId());

        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);
        Assertions.assertEquals((leadOptional.get()).getNome(), lead.getNome());
        Mockito.verify(leadRepository, Mockito.times(1)).findById(lead.getId());

    }

    @Test
    public void testarBuscarTodosLeads() throws Exception{

        Iterable<Lead> leadIterable = Arrays.asList(lead, lead, lead);

        Mockito.when(leadRepository.findAll()).thenReturn(leadIterable);
        Iterable<Lead> leadObjeto = leadService.buscarTodosLeads();
        Assertions.assertEquals(leadObjeto, leadIterable);
        Mockito.verify(leadRepository, Mockito.times(1)).findAll();

    }

    @Test
    public void testarAtualizarLead(){

        Lead leadData = new Lead();

        leadData.setNome("Yuri");
        leadData.setEmail("yuri@gmail.com");
        leadData.setTipoDeLead(TipoDeLead.ORGANICO);
        leadData.setId(lead.getId());
        leadData.setProdutos(lead.getProdutos());

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        leadService.atualizarLead(leadData);

        Assertions.assertEquals("yuri@gmail.com", leadData.getEmail());
        Assertions.assertEquals("Yuri", leadData.getNome());
        Assertions.assertEquals(TipoDeLead.ORGANICO, leadData.getTipoDeLead());
    }
}