package com.br.lead.collector.controllers;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.LeadRepository;
import com.br.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.swing.text.html.Option;
import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadController.class)
public class LeadControllerTests {

    @MockBean
    LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    Lead lead;
    Produto produto;

    @BeforeEach
    public void iniciar(){
        lead = new Lead();
        lead.setNome("Vinicius");
        lead.setEmail("vinicius@gmail.com");
        lead.setTipoDeLead(TipoDeLead.FRIO);

        produto = new Produto();
        produto.setNome("Cafe");
        produto.setDescricao("Cafe e a melhor bebida");
        produto.setPreco(10.00);
        produto.setId(1);

        lead.setProdutos(Arrays.asList(produto));

    }

    @Test
    public void testarIncluirLead() throws Exception {

        Iterable<Produto> produtoIterable = Arrays.asList(produto);

        Mockito.when(leadService.adicionarLead(Mockito.any(Lead.class))).thenReturn(lead);
        //Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));
    }

//    @Test
//    public void testarBuscarTodosLeads() throws Exception {
//
//        Optional<Lead> leadOptional = Optional.of(lead);
//
//        Mockito.when(leadService.buscarTodosLeads().thenReturn(leadOptional));
//
//        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1"))
//                .andExpect(MockMvcResultMatchers.status().isOk());
//    }


    @Test
    public void testarDeletarLead() throws Exception {

        Mockito.when(leadService.deletarLead(Mockito.any(Lead.class))).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}