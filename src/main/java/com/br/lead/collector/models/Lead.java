package com.br.lead.collector.models;

import com.br.lead.collector.enums.TipoDeLead;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.awt.*;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nome_completo")
    @Size(min = 8, max = 100, message = "O nome deve ter entre 8 e 100 caracteres")
    private String nome;

    @Email(message = "O formato do e-mail é invalido")
    private String email;
    private TipoDeLead tipoDeLead;

    @ManyToMany
    private List<Produto> produtos;

    public Lead() {
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public Lead(String nome, String email, TipoDeLead tipoDeLead) {
        this.nome = nome;
        this.email = email;
        this.tipoDeLead = tipoDeLead;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipoDeLead getTipoDeLead() {
        return tipoDeLead;
    }

    public void setTipoDeLead(TipoDeLead tipoDeLead) {
        this.tipoDeLead = tipoDeLead;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}